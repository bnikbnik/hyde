# Hyde

Very simple templating tool for static web sites.

Hyde is in slow development, and I will add any features I find useful as I
need them for my own websites.

## Basic Functionality

Create a data folder next to the executable, and put your html files inside.

As of now, hyde will not inside subfolders in the data folder. This may be added
in a future version.

Any assets or files you don't need hyde to work on should be placed in a dist
folder, but you should take care not to name them like any file in the data
folder, because Hyde will overwrite them when done.

You must have one layout.html file that will contain all html that's supposed
to exist in all the pages, most commonly head tags, menus, footers etc.

Inside this layout file, place one {} sequence at the place you want the page
content to be inserted.

All the other pages only need to contain their specific html.

Running hyde will create the complete pages in the dist folder.
