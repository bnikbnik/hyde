#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#pragma comment(lib, "kernel32.lib")

struct MemoryBlock {
  void* data;
  size_t size;
};

struct LayoutFile {
  MemoryBlock contents;
  bool read_successfully = false;

  bool valid = false;
  size_t body_start;
  size_t body_end;
};

enum ParseStates {
  SCAN = 0,
  CONTENT_BRACKETS,
};

int main() {
  printf("It's Hyde.\n");

  CreateDirectoryA("dist", NULL);

  LayoutFile layout_file;

  {
    // layout file
    printf("Loading layout file\n");
    HANDLE file = CreateFileA("data\\layout.html", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);

    if ( file != INVALID_HANDLE_VALUE ) {
      LARGE_INTEGER file_size;

      if ( GetFileSizeEx(file, &file_size) ) {
        assert(file_size.QuadPart < 0xFFFFFFFF);
        uint32_t file_size_32 = (uint32_t)file_size.QuadPart;

        layout_file.contents.data = VirtualAlloc(NULL, file_size_32, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        ReadFile(file, layout_file.contents.data, file_size_32, NULL, NULL);
        layout_file.contents.size = (size_t)file_size_32;
        layout_file.read_successfully = true;

        ParseStates parse_state = SCAN;
        for ( size_t it = 0; it < layout_file.contents.size; it++ ) {
          char c = ((char *)(layout_file.contents.data))[it];

          switch(parse_state) {
            case SCAN:
              if ( c == '{' ) {
                parse_state = CONTENT_BRACKETS;
                layout_file.body_start = it;
              }

              break;
            case CONTENT_BRACKETS:
              if ( c == '}' ) {
                layout_file.body_end = it;
                layout_file.valid = true;

                goto parse_end;
              } else {
                parse_state = SCAN;
              }

              break;
          }
        }
parse_end:
        if ( layout_file.valid ) {
          printf("%zu-%zu\n", layout_file.body_start, layout_file.body_end);
        } else {
          printf("I couldn't find a {} in your layout file, so I don't know how to proceed. You need a {} in the place where your partial pages will be added.");

          return 1;
        }
      }

      CloseHandle(file);
    }

    if ( !layout_file.read_successfully ) {
      printf("Layout file is not present in the data directory.\n");
      printf("You have to have one layout.html file.\n");
      return 1;
    }
  }

  {
    WIN32_FIND_DATAA find_data;
    HANDLE find_handle = FindFirstFileA("data\\*", &find_data);

    if (find_handle != INVALID_HANDLE_VALUE) {
      do {
        if ( find_data.cFileName[0] == '.' ) { continue; }
        if ( strcmp(find_data.cFileName, "layout.html") == 0 ) { continue; } // skip layout file

        printf("Working on %s\n", find_data.cFileName);

        char in_file_name[MAX_PATH];
        sprintf_s(in_file_name, MAX_PATH, "data\\%s", find_data.cFileName);
        HANDLE in_file = CreateFileA(in_file_name, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
        MemoryBlock in_file_contents;

        if ( in_file != INVALID_HANDLE_VALUE ) {
          char out_file_name[MAX_PATH];
          sprintf_s(out_file_name, MAX_PATH, "dist\\%s", find_data.cFileName);
          HANDLE out_file = CreateFileA(out_file_name, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);

          if ( out_file != INVALID_HANDLE_VALUE ) {
            DWORD bytes_written;
            WriteFile(out_file, layout_file.contents.data, layout_file.body_start, &bytes_written, NULL);
            // @Todo error checking maybe

            LARGE_INTEGER in_file_size;
            if ( GetFileSizeEx(in_file, &in_file_size) ) {
              assert(in_file_size.QuadPart < 0xFFFFFFFF);
              uint32_t in_file_size_32 = (uint32_t)in_file_size.QuadPart;

              in_file_contents.data = VirtualAlloc(NULL, in_file_size_32, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
              ReadFile(in_file, in_file_contents.data, in_file_size_32, NULL, NULL);
              in_file_contents.size = in_file_size_32;
            }

            WriteFile(out_file, in_file_contents.data, in_file_contents.size, &bytes_written, NULL);

            WriteFile(out_file,
                ((char*)layout_file.contents.data + layout_file.body_end + 1),
                (layout_file.contents.size - layout_file.body_end),
                &bytes_written, NULL);

            CloseHandle(out_file);
          }

          VirtualFree(in_file_contents.data, 0, MEM_RELEASE);
          CloseHandle(in_file);
        }
      } while ( FindNextFileA(find_handle, &find_data) );

      FindClose(find_handle);
    } else {
      printf("Sorry, we failed.");
    }

  }

  VirtualFree(layout_file.contents.data, 0, MEM_RELEASE);
  return 0;
}
