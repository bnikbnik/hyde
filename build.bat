@echo off
if "%1" == "opt" (
  clang++ hyde.cpp -O2 -o hyde.exe
) else (
  clang++ hyde.cpp -o hyde.exe -g -gcodeview
)
